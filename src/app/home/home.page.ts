import { Component } from '@angular/core';
import { IonRouterOutlet, Platform } from '@ionic/angular';
import { Plugins } from '@capacitor/core';

const { App } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  darkMode = false;
  invalid = false;
  txtValue = [];
  buttons = [
    '+', '-', '*', '/',
    '7', '8', '9', '←',
    '4', '5', '6', 'c',
    '1', '2', '3', '=',
    '(', '0', ')', '.',
  ];

  constructor(
    private platform: Platform,
    private routerOutlet: IonRouterOutlet
  ) {
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (!this.routerOutlet.canGoBack()) {
        App.exitApp();
      }
    });
  }

  toggleDarkMode() {
    this.darkMode = !this.darkMode;
    document.body.classList.toggle('dark', this.darkMode);
  }

  getTxtValue(): string {
    return this.txtValue.join('');
  }

  input(button: string) {
    if (this.invalid) {
      this.invalid = false;
      this.txtValue = [];
    }

    if (button === '←') {
      this.txtValue.pop();
    } else if (button.toLowerCase() === 'c') {
      this.txtValue = [];
    } else if (button === '=') {
      this.txtValue = [this.calculate()];
    } else {
      this.txtValue.push(button);
    }
  }

  calculate() {
    try {
      const result: number = eval(this.getTxtValue());
      return result.toString();
    } catch (error) {
      this.invalid = true;
      return 'invalid';
    }
  }

}
